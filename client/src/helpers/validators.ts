// export const required = (val) => !!val;
//
// export const minLength = (length) => (val) => val.length >= length;
export interface ValidatorResponce {
  isValid: boolean;
  error: string;
}
export const required = (val: string): ValidatorResponce => {
  const isValid = !!val;
  return {
    isValid,
    error: !isValid ? "Поле обов'язкове" : "",
  };
};

export const isEmail = (val: string): ValidatorResponce => {
  const regex =
    /^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
  const isValid = regex.test(val);
  return {
    isValid,
    error: !isValid ? "Пошта якась дивна" : "",
  };
};

export const minLength = (length: number) => (val: string) => {
  const isValid = val.length >= length;
  return {
    isValid,
    error: !isValid
      ? `Не будь ледачим, напиши більше ніж ${length} символів`
      : "",
  };
};

export const maxLength = (length: number) => (val: string) => {
  const isValid = val.length <= length;
  return {
    isValid,
    error: !isValid ? `Це вже занадто, максимум ${length} символів` : "",
  };
};
