import { makeAutoObservable } from "mobx";
import { IUser, IUserData } from "../types/user";

class UserStore {
  _isAuth: boolean;
  _user: IUserData;
  constructor() {
    this._isAuth = false;
    this._user = {} as IUserData;
    makeAutoObservable(this);
  }

  setIsAuth(val: boolean) {
    this._isAuth = val;
  }

  setUser(user: IUserData) {
    this._user = user;
  }

  get isAuth(): boolean {
    return this._isAuth;
  }

  get user(): IUserData {
    return this._user;
  }
  get profile(): IUser {
    return this._user.profile;
  }
  get token(): string {
    return this._user.token;
  }
}

export default new UserStore();
