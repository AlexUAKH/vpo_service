export interface IAdditionalOption {
  item: string;
  id: number;
}
