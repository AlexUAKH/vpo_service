import { EDistributionPoint } from "../enums/distribution-point";
import { ERecipientGroups } from "../enums/recipient-group";
import dayjs from "../plugins/dayjs";
import { IAdditionalOption } from "./additonalOptions";

export interface IRecipient {
  name: string;
  _id?: string;
  phoneNumber: string;
  city: string;
  comments: string;
  lastReceivedDate: string | Date | dayjs.Dayjs | null;
  isReceived: boolean;
  distributionPoint: EDistributionPoint;
  blackList: boolean;
  group: ERecipientGroups;
  age: number;
  children: number;
  additional: IAdditionalOption[];
}
