export type Statistic = {
  day: number;
  month: number;
  year: number;
  total: number;
};
