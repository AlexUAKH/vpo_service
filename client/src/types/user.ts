export interface IUser {
  email: string;
  id: string;
}

export interface IUserData {
  token: string;
  refreshToken: string;
  tokenExpire: number;
  refreshTokenExpire: number;
  profile: IUser;
}
