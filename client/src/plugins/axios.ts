import axios, {
  AxiosError,
  AxiosRequestConfig,
  AxiosRequestHeaders,
} from "axios";
import jwt from "jwt-decode";
import { toast } from "react-toastify";
import { useLogout } from "../hooks/logout";
import { IUserData } from "../types/user";

const logout = useLogout();

const headers: AxiosRequestHeaders = {
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Credentials": "true",
};

const http = axios.create({
  baseURL:
    process.env.NODE_ENV === "development"
      ? process.env.REACT_APP_BASE_URL_DEV
      : process.env.REACT_APP_BASE_URL,
  headers,
});

const authHttp = axios.create({
  baseURL:
    process.env.NODE_ENV === "development"
      ? process.env.REACT_APP_BASE_URL_DEV
      : process.env.REACT_APP_BASE_URL,
  headers,
});

authHttp.interceptors.request.use(
  (config: AxiosRequestConfig): AxiosRequestConfig => {
    const user = localStorage.getItem("user") || "{}";
    const token = JSON.parse(user).token;
    // @ts-ignore
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  }
);

// TODO: move interceptor and toast messages to own class after finish all errors listing
http.interceptors.response.use(
  function (response) {
    // Any 2xx code
    return response;
  },
  function (error: AxiosError<any>) {
    return handleError(error);
  }
);

authHttp.interceptors.response.use(
  function (response) {
    // Any 2xx code
    return response;
  },
  function (error: AxiosError<any>) {
    return handleError(error);
  }
);

export { http, authHttp };

function handleError(error: AxiosError<any>) {
  // Not 2xx code
  // const user = JSON.parse(localStorage.getItem("user") || "{}");
  // const refreshToken = user.refreshToken;
  switch (error.response?.status) {
    case 401:
      // if (refreshToken * 1000 > Date.now()) {
      //   console.log("refresh ++");

      //   refresh(error);
      // } else {
      toast.error("Упс. Немаэ доступу.  Мабуть ти забув авторизуватися.", {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
      localStorage.removeItem("user");
      logout();

      // window.location.href = "/";
      // }
      break;
    case 404:
      toast.error("Server warning 404: Requested url is not exists", {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
      break;
    case 500:
      // Handle 500 here
      toast.error(`Server error 500:  Error: ${error.response?.data.message}`, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
      break;
  }
  return Promise.reject(error);
}

async function refresh(err: AxiosError<any>) {
  const originalReq: any = err.config;
  if (originalReq && !originalReq._isretry) {
    originalReq._isretry = true;
    try {
      const res = await axios.get(
        `${process.env.REACT_APP_BASE_URL}/auth/refresh`,
        {
          withCredentials: true,
        }
      );

      const token = res.data.token;
      const decodedToken = jwt(token);
      const refreshToken = res.data.refresh_token;
      const decodedRefreshToken = jwt(refreshToken);
      const { exp: tokenExpire, profile }: any = decodedToken;
      const { exp: refreshTokenExpire }: any = decodedRefreshToken;
      const _user: IUserData = {
        token,
        refreshToken,
        tokenExpire,
        refreshTokenExpire,
        profile,
      };

      localStorage.setItem("user", JSON.stringify(_user));
      return await authHttp.request(originalReq);
    } catch (e) {
      console.log("");
    }
  } else {
    throw err;
  }
}
