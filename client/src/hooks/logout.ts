import UserStore from "../store/UserStore";
import { IUserData } from "../types/user";

export const useLogout = () => {
  return () => {
    localStorage.removeItem("user");
    UserStore.setIsAuth(false);
    UserStore.setUser({} as IUserData);
  };
};
