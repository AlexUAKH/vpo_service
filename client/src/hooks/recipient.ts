import { EDistributionPoint } from "../enums/distribution-point";
import { ERecipientGroups } from "../enums/recipient-group";
import { IAdditionalOption } from "../types/additonalOptions";
import { IRecipient } from "../types/recipient";

// export interface IUseRecipientInput {}
export interface IUseRecipientOutput {
  getInitialRecipient: (distributionPoint: EDistributionPoint) => IRecipient;
}

export const useRecipient = (/*input: IUseRecipientInput*/) => {
  function getInitialRecipient(
    distributionPoint: EDistributionPoint
  ): IRecipient {
    return {
      name: "",
      city: "",
      phoneNumber: "",
      comments: "",
      lastReceivedDate: "",
      isReceived: false,
      distributionPoint,
      blackList: false,
      group: ERecipientGroups.Default,
      age: 0,
      children: 0,
      additional: [] as IAdditionalOption[],
    };
  }

  return {
    getInitialRecipient,
  } as IUseRecipientOutput;
};
