import { useContext, useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import StatisticWidget from "./components/StatisticsWidget";
import TheHeader from "./components/TheHeader";
import { Context } from "./index";
import Main from "./pages/main/Main";
import QueryList from "./pages/query-list/QueryList";
import { statisticDataService } from "./services/statistic-data-service";
import { Statistic } from "./types/statistic";
import { IUserData } from "./types/user";

function App() {
  const { user } = useContext(Context);
  const [statistic, setStatistic] = useState<Statistic>({
    day: 0,
    month: 0,
    year: 0,
    total: 0,
  });

  async function getStatistic() {
    try {
      const { data } = await statisticDataService.getStatistic();
      setStatistic(data);
    } catch (error: any) {
      toast.warning(`${error?.response?.data?.message}`, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    }
  }

  useEffect(() => {
    getStatistic();
  }, []);

  useEffect(() => {
    const _user = localStorage.getItem("user");
    if (_user) {
      const parsedUser = JSON.parse(_user);
      if (parsedUser.tokenExpire * 1000 > Date.now()) {
        user.setUser(parsedUser);
        user.setIsAuth(true);
      } else {
        // localStorage.removeItem("user");
        user.setUser({} as IUserData);
        user.setIsAuth(false);
      }
    }
  }, []);

  return (
    <Router>
      <div className="App">
        <ToastContainer />
        <TheHeader />
        <div className="my-2 my-lg-4 d-flex justify-content-center">
          <StatisticWidget statistic={statistic} />
        </div>

        <Routes>
          <Route path="/" element={<Main updated={() => getStatistic()} />} />
          <Route path="/list" element={<QueryList />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
