import { EWeeks } from "../enums/weeks";
import { EColors } from "../enums/colors";

export const weekToColor = {
  [EWeeks.OneWeek]: EColors.Red,
  [EWeeks.TwoWeeks]: EColors.Yellow,
  [EWeeks.More]: EColors.Green,
};
