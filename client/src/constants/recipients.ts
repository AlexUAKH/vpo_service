import { EDistributionPoint } from "../enums/distribution-point";
import { ERecipientGroups } from "../enums/recipient-group";
import { ESortRecipient } from "../enums/sort";

export const distributionPointOptions = [
  { value: EDistributionPoint.NewLife, label: EDistributionPoint.NewLife },
  { value: EDistributionPoint.Filter, label: EDistributionPoint.Filter },
  {
    value: EDistributionPoint.Gorodinskogo,
    label: EDistributionPoint.Gorodinskogo,
  },
  {
    value: EDistributionPoint.Reconciliation,
    label: EDistributionPoint.Reconciliation,
  },
];

export const recipientSortingOptions = [
  {
    value: ESortRecipient.Ascending,
    label: "Недавні",
  },
  {
    value: ESortRecipient.Descending,
    label: "Давніші",
  },
  {
    value: ESortRecipient.Group,
    label: "По групам",
  },
];

export const recipientGroupOptions = [
  {
    value: ERecipientGroups.Default,
    label: "Загальна",
  },
  {
    value: ERecipientGroups.One,
    label: "Перша 1",
  },
  {
    value: ERecipientGroups.Two,
    label: "Друга 2",
  },
  {
    value: ERecipientGroups.Three,
    label: "Третя 3",
  },
];

export const RecipientAdditionalOptions = [
  { item: "Одеяло", id: 1 },
  { item: "Чайник", id: 2 },
];
