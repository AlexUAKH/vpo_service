import { FC } from "react";
import { Modal } from "react-bootstrap";
import LoginForm from "./LoginForm";

interface LoginModalProps {
  show: boolean;
  close: () => void;
}

const LoginModal: FC<LoginModalProps> = ({ show, close }) => {
  const handleClose = () => {
    close();
  };

  return (
    <Modal show={show} fullscreen={"md-down"} onHide={handleClose}>
      <Modal.Header closeButton></Modal.Header>
      <Modal.Title className="text-center">Увійти</Modal.Title>
      <Modal.Body>
        <LoginForm abort={handleClose} success={handleClose} />
      </Modal.Body>
    </Modal>
  );
};

export default LoginModal;
