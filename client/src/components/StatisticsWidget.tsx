import { FC } from "react";
import { Badge, ProgressBar } from "react-bootstrap";
import { Statistic } from "../types/statistic";

interface StatisticWidgetProps {
  statistic: Statistic;
}

const StatisticWidget: FC<StatisticWidgetProps> = ({ statistic }) => {
  const min = 0,
    max = statistic.total;
  return (
    <div>
      <h6 className="p-2 d-flex align-items-center m-0 gap-2">
        <span>
          Статистика: за рік <Badge bg="success">{statistic.year}</Badge>
        </span>
        <span>
          за останій місяць <Badge bg="warning"> {statistic.month}</Badge>
        </span>
        <span>
          за сьогодні <Badge bg="danger">{statistic.day}</Badge>
        </span>
      </h6>

      <div className="d-flex align-items-center gap-2 p-2">
        <Badge bg="secondary">{min}</Badge>

        <ProgressBar className="flex-fill">
          <ProgressBar
            striped
            variant="success"
            now={statistic.year}
            key={1}
            max={max}
          />
          <ProgressBar
            striped
            variant="warning"
            now={statistic.month}
            key={2}
            max={max}
          />
          <ProgressBar
            striped
            variant="danger"
            now={statistic.day}
            key={3}
            max={max}
          />
        </ProgressBar>

        <Badge bg="secondary">{max}</Badge>
      </div>
    </div>
  );
};
export default StatisticWidget;
