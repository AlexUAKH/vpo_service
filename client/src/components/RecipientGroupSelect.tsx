import { Form } from "react-bootstrap";
import { recipientGroupOptions } from "../constants/recipients";
import { ERecipientGroups } from "../enums/recipient-group";

export interface RecipientGroupSelectProps {
  // tabIndex?: number | string;
  selected: ERecipientGroups;
  handleSelect: (value: any) => void;
}

export function RecipientGroupSelect(props: RecipientGroupSelectProps) {
  const options = recipientGroupOptions;

  function handleSelect(e: any) {
    e.preventDefault();
    props.handleSelect(e.target.value);
  }

  return (
    <Form.Select value={props.selected} onChange={handleSelect}>
      {options.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </Form.Select>
  );
}
