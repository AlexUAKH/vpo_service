import dayjs from "dayjs";
import Multiselect from "multiselect-react-dropdown";
import { FC, useEffect, useMemo, useState } from "react";
import { Button, Card, Form, Modal, Spinner } from "react-bootstrap";
import { toast } from "react-toastify";
import { DATE_FORMAT } from "../constants/formats";
import { RecipientAdditionalOptions } from "../constants/recipients";
import { ERecipientGroups } from "../enums/recipient-group";
import { recipientDataService } from "../services/recipients-data-service";
import { IRecipient } from "../types/recipient";
import { DistributionPointSelect } from "./DistributionPointSelect";
import { RecipientGroupSelect } from "./RecipientGroupSelect";

interface RecipientEditModalProps {
  show: boolean;
  recipient: IRecipient;
  close: () => any;
  update: (recipient: IRecipient) => void;
}

const RecipientEditModal: FC<RecipientEditModalProps> = ({
  show,
  recipient,
  close,
  update,
}) => {
  const [user, setUser] = useState<IRecipient>({} as IRecipient);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setUser({ ...recipient });
  }, [recipient]);

  const isCreateDisabled = useMemo(() => {
    const { lastReceivedDate, comments, ...rest } = user;

    return Object.values(rest).some((value) => value === "");
  }, [user]);

  const handleSave = async (e: any) => {
    e.preventDefault();
    try {
      setLoading(true);

      const trimmedObject = Object.fromEntries(
        Object.entries(user).map(([key, value]) => [
          key,
          typeof value === "string" ? value.trim() : value,
        ])
      ) as IRecipient;

      const normalizedPhone =
        "0" + trimmedObject.phoneNumber.replace(/\D/g, "").slice(-9);

      const currentDateTimestamp = String(new Date().toISOString());
      const lastReceivedDate = user.lastReceivedDate
        ? user.lastReceivedDate
        : user.isReceived
        ? currentDateTimestamp
        : null;

      // const lastReceivedDate = user.isReceived
      // ? user.lastReceivedDate
      // : null

      const result = await recipientDataService.update({
        ...trimmedObject,
        phoneNumber: normalizedPhone,
        lastReceivedDate,
        age: user.age,
        children: user.children,
        additional: user.additional,
      });

      update(result);
      handleClose();
      // setUser({ ...initUserData });
    } catch (err: any) {
      toast.warning(err.response.data.error, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    } finally {
      setLoading(false);
    }
  };

  const handleClose = () => {
    close();
  };

  return (
    <Modal show={show} fullscreen={"md-down"} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{user.name || "Створити"}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {Object.keys(user).length > 0 && (
          <Card>
            <Card.Body>
              <Form className="d-flex flex-column gap-2">
                <Form.Group>
                  <Form.Label>
                    ПІБ:
                    <span className="text-danger">*</span>
                  </Form.Label>
                  <Form.Control
                    tabIndex={0}
                    value={user.name || ""}
                    onChange={(event) =>
                      setUser({ ...user, name: event.target.value })
                    }
                    placeholder="Введить ПІБ"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>
                    Телефон:
                    <span className="text-danger">*</span>
                  </Form.Label>
                  <Form.Control
                    tabIndex={0}
                    value={user.phoneNumber || ""}
                    onChange={(event) =>
                      setUser({
                        ...user,
                        phoneNumber: event.target.value,
                      })
                    }
                    placeholder="Введить телефон"
                  />
                </Form.Group>
                <div className="d-flex justify-content-between">
                  <Form.Group>
                    <Form.Label>
                      Місто:
                      <span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={user.city || ""}
                      onChange={(event) =>
                        setUser({ ...user, city: event.target.value })
                      }
                      placeholder="Введить місто"
                    />
                  </Form.Group>

                  <Form.Group className="mx-2 w-25">
                    <Form.Label>Років:</Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={user.age}
                      onChange={(event) => {
                        setUser({
                          ...user,
                          age: Number(
                            event.target.value.replace(/[^0-9]/g, "")
                          ),
                        });
                      }}
                      placeholder="0"
                    />
                  </Form.Group>

                  <Form.Group className="w-25">
                    <Form.Label>Діти:</Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={user.children}
                      onChange={(event) => {
                        setUser({
                          ...user,
                          children: Number(
                            event.target.value.replace(/[^0-9]/g, "")
                          ),
                        });
                      }}
                      placeholder="0"
                    />
                  </Form.Group>
                </div>

                <Form.Group>
                  <Form.Label>Коментар:</Form.Label>
                  <Form.Control
                    tabIndex={0}
                    value={user.comments || ""}
                    onChange={(event) =>
                      setUser({ ...user, comments: event.target.value })
                    }
                    placeholder="Введить коментар"
                  />
                </Form.Group>
                <div className="d-flex justify-content-between">
                  <Form.Group>
                    <Form.Label>Пункт видачі:</Form.Label>
                    <DistributionPointSelect
                      selected={user.distributionPoint || ""}
                      handleSelect={(value) =>
                        setUser({ ...user, distributionPoint: value })
                      }
                    />
                  </Form.Group>

                  <Form.Group className="mx-1">
                    <Form.Label>Дата видачі:</Form.Label>
                    <Form.Control
                      disabled={!user.isReceived}
                      type="date"
                      value={
                        dayjs(user.lastReceivedDate).format(DATE_FORMAT) || ""
                      }
                      onChange={(e) =>
                        setUser({
                          ...user,
                          lastReceivedDate: dayjs(e.target.value).format(
                            DATE_FORMAT
                          ),
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Номер групи:</Form.Label>
                    <RecipientGroupSelect
                      selected={Number(user.group) || ERecipientGroups.Default}
                      handleSelect={(value) =>
                        setUser({ ...user, group: Number(value) })
                      }
                    />
                  </Form.Group>
                </div>
                <Form.Check
                  checked={user.isReceived}
                  onChange={() =>
                    setUser({
                      ...user,
                      isReceived: !user.isReceived,
                    })
                  }
                  label="Отримав продуктовий набір"
                />
                <Multiselect
                  selectedValues={user.additional}
                  options={RecipientAdditionalOptions}
                  displayValue="item"
                  emptyRecordMsg="Немає плюшок"
                  showArrow
                  placeholder="Додактово отримав"
                  closeIcon="cancel"
                  closeOnSelect
                  onSelect={(selectedList) => {
                    setUser({ ...user, additional: selectedList });
                  }}
                  onRemove={(selectedList) => {
                    setUser({ ...user, additional: selectedList });
                  }}
                />
              </Form>
            </Card.Body>
            <Modal.Footer>
              <Button
                tabIndex={1}
                variant="secondary"
                className="mx-1"
                onClick={handleClose}
              >
                Скасувати
              </Button>
              <Button
                tabIndex={0}
                variant="primary"
                disabled={isCreateDisabled || loading}
                onClick={handleSave}
              >
                {loading && (
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                )}
                &nbsp;Сберегти&nbsp;
                {loading && (
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                  />
                )}
              </Button>
            </Modal.Footer>
          </Card>
        )}
      </Modal.Body>
    </Modal>
  );
};

export default RecipientEditModal;
