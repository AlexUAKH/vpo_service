import { FC, useContext, useState } from "react";
import { Button, Form, Spinner } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { Context } from "../index";
import { userService } from "../services/user-service";

const emailRegexp =
  /^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

export interface ILoginData {
  email: string;
  password: string;
}
interface LoginFormProps {
  abort: () => void;
  success?: () => void;
}

const LoginForm: FC<LoginFormProps> = ({ abort, success }) => {
  const { user } = useContext(Context);
  const [submiting, setSubmiting] = useState<boolean>(false);
  const {
    register,
    reset,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<ILoginData>({
    mode: "all",
    delayError: 1000,
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const onSubmit = async (data: ILoginData) => {
    setSubmiting(true);
    try {
      const _user = await userService.login(data);

      user.setUser(_user);
      user.setIsAuth(true);
      localStorage.setItem("user", JSON.stringify(_user));
      toast.success(`З поверненням, ${_user.profile.email}`, {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
      success && success();
    } catch (err: any) {
      toast.error(err.response.data.error, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    } finally {
      setSubmiting(false);
    }
  };

  const onAbort = () => {
    reset();
    abort();
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3" controlId="formLoginEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          isInvalid={!!errors.email}
          {...register("email", {
            required: "Поле обов'язкове",
            pattern: {
              value: emailRegexp,
              message: "Щось не схоже на пошту",
            },
          })}
        />
        <Form.Control.Feedback type="invalid">
          {errors.email?.message}
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formLoginPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          isInvalid={!!errors.password}
          {...register("password", {
            required: "Поле обов'язкове",
            minLength: {
              value: 6,
              message: "Не будь ледачим, треба хочаб 6 символів ",
            },
          })}
        />
        <Form.Control.Feedback type="invalid">
          {errors.password?.message}
        </Form.Control.Feedback>
      </Form.Group>

      <div className="d-flex justify-content-between">
        <Button variant="outline-danger" className="px-4" onClick={onAbort}>
          Відбій
        </Button>
        <Button
          variant="primary"
          type="submit"
          onClick={handleSubmit(onSubmit)}
          disabled={!isValid || submiting}
          className="px-4"
        >
          {submiting && (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
              className="mr-2"
            />
          )}
          Увійти
        </Button>
      </div>
    </Form>
  );
};

export default LoginForm;
