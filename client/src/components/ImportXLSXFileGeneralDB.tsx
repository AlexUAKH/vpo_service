import { useState } from "react";
import { Alert, Button, Form, Modal, Spinner } from "react-bootstrap";
import { toast } from "react-toastify";
import readXlsxFile from "read-excel-file";
import dayjs from "../plugins/dayjs";
import { recipientDataService } from "../services/recipients-data-service";
import { IRecipient } from "../types/recipient";

export interface Props {
  imported: () => void;
}

export default function ImportXLSXFileTOGeneralDB(props: Props) {
  const [file, setFile] = useState<any>(undefined);
  const [visible, setVisible] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  function handleOpen() {
    setVisible(true);
  }
  function handleClose() {
    setVisible(false);
  }
  function chooseFile(e: any) {
    setFile(e.target.files[0]);
  }

  async function exportFromXLSX(e: any) {
    e.preventDefault();

    try {
      setLoading(true);
      const fileName = file.name.split(".");
      const ext = fileName[fileName.length - 1];
      let formattedData = [] as IRecipient[];
      if (ext === "xlsx") {
        const data = await readXlsxFile(file);

        formattedData = data.map<IRecipient>(
          ([
            lastReceivedDate,
            name,
            phone,
            city,
            ,
            comments,
            distributionPoint,
            isReceived,
          ]) => {
            if (!lastReceivedDate) {
              throw new Error(
                `Дата останнього отримання у ${name} повина бути присутня`
              );
            }

            const phoneNumber = formatPhoneNumber(String(phone));

            return {
              lastReceivedDate: dayjs(String(lastReceivedDate)).utc().format(),
              name,
              phoneNumber,
              city,
              comments,
              distributionPoint,
              isReceived: !!isReceived,
            } as IRecipient;
          }
        );
        // await recipientDataService.importToDB(formattedData);
      } else if (ext === "json") {
        // TODO move to json file read service
        const reader = new FileReader();
        reader.readAsText(file);
        reader.onload = async (event) => {
          const str: any = event.target?.result;
          const json = await JSON.parse(str);

          formattedData = json.map((el: IRecipient) => {
            const phoneNumber = formatPhoneNumber(el.phoneNumber);
            return {
              ...el,
              phoneNumber,
              isReceived: !!el.isReceived,
            };
          });
        };
      } else {
        throw new Error(`Файл формату *.${ext} не підтримується`);
      }
      await recipientDataService.importToDB(formattedData);
    } catch (err: any) {
      toast.warning(err.response.data.error, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    } finally {
      props.imported();
      setLoading(false);
    }
  }

  function formatPhoneNumber(phone: string) {
    return phone === "null"
      ? null
      : "0" + String(phone).replace(/\D/g, "").slice(-9);
  }

  return (
    <>
      <Button variant="info" onClick={handleOpen} className="text-nowrap">
        Імпортувати <strong>(&nbsp;.xlsx</strong> або
        <strong>&nbsp;.json)</strong>
      </Button>

      <Modal show={visible} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Створити нового одержувача</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {loading ? (
            <Spinner animation="border" />
          ) : (
            <>
              <Alert variant="warning">
                <div>Стовбці повинні бути правильно заповнені</div>
                <div>{"A -> дата отримання;"}</div>
                <div>{"B -> ПІБ;"}</div>
                <div>{"С -> номер телевону;"}</div>
                <div>{"D -> місто;"}</div>
                <div>{"F -> коментар;"}</div>
                <div>{"G -> пункт роздачі;"}</div>
                <div>{"H -> чи отримував;"}</div>
              </Alert>

              <Form.Group>
                <Form.Label>
                  Виберіть файл формату <strong>.xlsx</strong> або
                  <strong>&nbsp;.json</strong>
                </Form.Label>

                <Form.Control
                  type="file"
                  accept=".xlsx, .json"
                  onChange={chooseFile}
                />
              </Form.Group>

              <Button
                variant="success"
                disabled={!file}
                className="w-100 mt-3"
                onClick={exportFromXLSX}
              >
                Імпортувати
              </Button>
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
}
