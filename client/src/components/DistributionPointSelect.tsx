import { Form } from "react-bootstrap";
import { distributionPointOptions } from "../constants/recipients";
import { EDistributionPoint } from "../enums/distribution-point";

export interface DistributionPointSelectProps {
  // tabIndex?: number | string;
  selected: EDistributionPoint | string;
  handleSelect: (value: any) => void;
}

export function DistributionPointSelect(props: DistributionPointSelectProps) {
  const options = distributionPointOptions;

  function handleSelect(e: any) {
    e.preventDefault();
    props.handleSelect(e.target.value);
  }

  return (
    <Form.Select value={props.selected} onChange={handleSelect}>
      {options.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </Form.Select>
  );
}
