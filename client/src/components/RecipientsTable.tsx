import { useState } from "react";
import { Badge, Button, Table } from "react-bootstrap";
import { DATE_FORMAT } from "../constants/formats";
import { weekToColor } from "../constants/week-to-color";
import { EColors } from "../enums/colors";
import { EDistributionPoint } from "../enums/distribution-point";
import { EWeeks } from "../enums/weeks";
import dayjs from "../plugins/dayjs";
import { recipientDataService } from "../services/recipients-data-service";
import { IAdditionalOption } from "../types/additonalOptions";
import { IRecipient } from "../types/recipient";
import RecipientEditModal from "./RecipientEditModal";

export interface IRecipientsTableProps {
  selectedPoint: EDistributionPoint;
  recipientList: IRecipient[];
  handleUpdateUserList: (recipientList: IRecipient[]) => void;
}

export const RecipientsTable = (props: IRecipientsTableProps) => {
  const [showEdit, setShowEdit] = useState<boolean>(false);
  const [editingRecipient, setEditingRecipient] = useState<IRecipient>(
    {} as IRecipient
  );

  async function handleUpdateUser(recipient: IRecipient, e: any) {
    e.preventDefault();

    try {
      const updatedRecipient = await recipientDataService.update({
        ...recipient,
        distributionPoint: props.selectedPoint,
      });

      updateList(updatedRecipient);
      // const updatedUserList = [...props.recipientList];
      // const searchIndex = updatedUserList.findIndex(
      //   (r) => r._id === recipient._id
      // );
      // updatedUserList.splice(searchIndex, 1, updatedRecipient);
      // props.handleUpdateUserList(updatedUserList);
    } catch (e) {
      console.error(e);
    }
  }

  function updateList(updatedRecipient: IRecipient) {
    const updatedUserList = [...props.recipientList];
    const searchIndex = updatedUserList.findIndex(
      (r) => r._id === updatedRecipient._id
    );
    updatedUserList.splice(searchIndex, 1, updatedRecipient);
    props.handleUpdateUserList(updatedUserList);
  }

  function handleEdit(recipient: IRecipient) {
    setEditingRecipient(recipient);
    setShowEdit(true);
  }

  async function handelDelete(recipient: IRecipient, e: any) {
    e.preventDefault();

    if (
      !confirm(`Ви впевнені що хочете видалити отримувача ${recipient.name}`)
    ) {
      return;
    }

    try {
      await recipientDataService.delete(recipient._id || "");
      const updatedRecipientList = [...props.recipientList];
      const searchIndex = updatedRecipientList.findIndex(
        (item) => item._id === recipient._id
      );
      updatedRecipientList.splice(searchIndex, 1);
      props.handleUpdateUserList(updatedRecipientList);
    } catch (e) {
      console.error(e);
    }
  }

  function getBackgroundColor(recipient: IRecipient): EColors {
    // in black list
    if (recipient.blackList) {
      return EColors.Black;
    }

    // якщо не отримував
    if (!recipient.isReceived) {
      return EColors.Default;
    }

    const now = dayjs(new Date());
    const currentDate = dayjs(recipient.lastReceivedDate);
    const oneWeekAgo = dayjs(now).subtract(7, "days");
    const twoWeeksAgo = dayjs(now).subtract(14, "days");

    if (currentDate <= now && currentDate >= oneWeekAgo) {
      return weekToColor[EWeeks.OneWeek];
    }
    if (currentDate < oneWeekAgo && currentDate >= twoWeeksAgo) {
      return weekToColor[EWeeks.TwoWeeks];
    }
    if (currentDate < twoWeeksAgo) {
      return weekToColor[EWeeks.More];
    }

    return weekToColor[EWeeks.More];
  }

  function getDate(date: null | string | Date | dayjs.Dayjs): string | null {
    return date ? dayjs(date).format(DATE_FORMAT) : date;
  }

  return (
    <>
      <div className="d-flex justify-content-center gap-2 mt-3 mb-1">
        <span>Отримував</span>
        <Badge bg="success">2+</Badge>
        <Badge bg="warning">1</Badge>
        <Badge bg="danger">0</Badge>
        <span>тижнів назад</span>
      </div>
      <div className="d-flex justify-content-center gap-3 mt-3 mb-1">
        <div>
          <span className="me-2">Ще не отримував</span>
          <Badge bg="secondary">--</Badge>
        </div>
        <div>
          <span className="me-2">У чонрному списку</span>
          <Badge bg="dark">--</Badge>
        </div>
      </div>
      <div className="table-responsive">
        <Table bordered hover>
          <thead>
            <tr>
              <th>ПІБ</th>
              <th>Номер</th>
              <th>Місто</th>
              <th>Коли отримував/зареєструвався</th>
              <th className="text-center">Додатково отримав(ла)</th>
              <th>Пункт отримання</th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            {(props.recipientList || []).map((recipient) => (
              <tr
                key={recipient._id}
                style={{ backgroundColor: getBackgroundColor(recipient) }}
                className="text-white"
                onDoubleClick={() => handleEdit(recipient)}
              >
                <td>{recipient.name}</td>
                <td>{recipient.phoneNumber}</td>
                <td>{recipient.city}</td>
                <td>{getDate(recipient.lastReceivedDate)}</td>
                <td className="text-center">
                  {recipient.additional && recipient.additional.length > 0
                    ? recipient.additional.map((item: IAdditionalOption) => {
                        return (
                          <Badge
                            pill
                            bg="primary"
                            key={item.id}
                            className="mx-1"
                          >
                            {item.item}
                          </Badge>
                        );
                      })
                    : "-"}
                </td>
                <td>{recipient.distributionPoint}</td>
                <td className="text-center">
                  <span className="d-inline-flex gap-2">
                    <Button
                      size="sm"
                      variant="primary"
                      onClick={(e: any) =>
                        handleUpdateUser(
                          {
                            ...recipient,
                            lastReceivedDate: new Date().toISOString(),
                            isReceived: true,
                          },
                          e
                        )
                      }
                    >
                      Отримав
                    </Button>

                    <Button
                      size="sm"
                      variant="secondary"
                      onClick={(e: any) =>
                        handleUpdateUser(
                          { ...recipient, blackList: !recipient.blackList },
                          e
                        )
                      }
                    >
                      {recipient.blackList
                        ? "До білого списку"
                        : "До чорного списку"}
                    </Button>

                    <Button
                      size="sm"
                      variant="danger"
                      onClick={(e: any) => handelDelete(recipient, e)}
                    >
                      Видалити
                    </Button>
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <RecipientEditModal
        show={showEdit}
        recipient={editingRecipient}
        close={() => setShowEdit(false)}
        update={updateList}
      />
    </>
  );
};
