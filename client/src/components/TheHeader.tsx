import { observer } from "mobx-react-lite";
import { FC, useContext, useState } from "react";
import { Button, Container, Nav, Navbar } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import { useLogout } from "../hooks/logout";
import { Context } from "../index";
import LoginModal from "./LoginModal";

const links = [
  { id: 1, path: "/", title: "Головна" },
  { id: 2, path: "/list", title: "Отримувачи" },
];

const TheHeader: FC = () => {
  const { user } = useContext(Context);
  const logout = useLogout();
  const location = useLocation();

  const [showLogin, setShowLogin] = useState<boolean>(false);

  const handleUser = () => {
    if (!user.isAuth) {
      setShowLogin(true);
    } else {
      logout();
      toast.success("Повертайся скоріше", {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    }
  };

  return (
    <Navbar bg="light" expand="lg" sticky="top" as="header" collapseOnSelect>
      <Container fluid>
        <Navbar.Brand href="/">
          _&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            {links.map((link) => (
              <Nav.Link
                as={Link}
                key={link.id}
                to={link.path}
                active={location.pathname === link.path}
                disabled={location.pathname === link.path}
              >
                {link.title}
              </Nav.Link>
            ))}
          </Nav>
          <div>
            {user.isAuth && <span className="me-2">{user.profile.email}</span>}
          </div>
          <Button variant="outline-primary" onClick={() => handleUser()}>
            {!user.isAuth ? "Увійти" : "Вийти"}
          </Button>
        </Navbar.Collapse>
      </Container>
      <LoginModal
        show={showLogin}
        close={() => setShowLogin(false)}
      ></LoginModal>
    </Navbar>
  );
};
export default observer(TheHeader);
