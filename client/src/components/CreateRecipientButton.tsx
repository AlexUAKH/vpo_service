import dayjs from "dayjs";
import Multiselect from "multiselect-react-dropdown";
import { useMemo, useState } from "react";
import { Button, Card, Form, Modal, Spinner } from "react-bootstrap";
import { toast } from "react-toastify";
import { DATE_FORMAT } from "../constants/formats";
import { RecipientAdditionalOptions } from "../constants/recipients";
import { EDistributionPoint } from "../enums/distribution-point";
import { useRecipient } from "../hooks/recipient";
import { recipientDataService } from "../services/recipients-data-service";
import { IRecipient } from "../types/recipient";
import { DistributionPointSelect } from "./DistributionPointSelect";
import { RecipientGroupSelect } from "./RecipientGroupSelect";

interface Props {
  selectedPoint: EDistributionPoint;
}

export default function CreateRecipientButton(props: Props) {
  const { getInitialRecipient } = useRecipient();
  const initUserData: IRecipient = useMemo(
    () => getInitialRecipient(props.selectedPoint),
    [props.selectedPoint]
  );

  const [isVisibleModal, setVisibleModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [newUser, setNewUser] = useState<IRecipient>({ ...initUserData });

  const isCreateDisabled = useMemo(() => {
    const { lastReceivedDate, comments, ...rest } = newUser;

    return Object.values(rest).some((value) => value === "");
  }, [newUser]);

  useMemo(
    () => setNewUser({ ...newUser, distributionPoint: props.selectedPoint }),
    [props.selectedPoint]
  );

  async function handleCreateNewUser(e: any) {
    e.preventDefault();

    try {
      setLoading(true);

      const trimmedObject = Object.fromEntries(
        Object.entries(newUser).map(([key, value]) => [
          key,
          typeof value === "string" ? value.trim() : value,
        ])
      ) as IRecipient;
      const normalizedPhone =
        "0" + trimmedObject.phoneNumber.replace(/\D/g, "").slice(-9);
      const currentDateTimestamp = String(new Date().toISOString());

      await recipientDataService.create({
        ...trimmedObject,
        phoneNumber: normalizedPhone,
        lastReceivedDate: newUser.isReceived ? currentDateTimestamp : null,
      });

      handleClose();
      setNewUser({ ...initUserData });
    } catch (err: any) {
      toast.error(err.response.data.error, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    } finally {
      setLoading(false);
    }
  }

  function handleClose() {
    setVisibleModal(false);
  }

  function handleShow() {
    setVisibleModal(true);
  }

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Створити нового одержувача
      </Button>

      <Modal show={isVisibleModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Створити нового одержувача</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {loading ? (
            <Spinner animation="border" />
          ) : (
            <Card>
              <Card.Body>
                <Form className="d-flex flex-column gap-2">
                  <Form.Group>
                    <Form.Label>
                      ПІБ:
                      <span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={newUser.name}
                      onChange={(event) =>
                        setNewUser({ ...newUser, name: event.target.value })
                      }
                      placeholder="Введить ПІБ"
                    />
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>
                      Телефон:
                      <span className="text-danger">*</span>
                    </Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={newUser.phoneNumber}
                      onChange={(event) =>
                        setNewUser({
                          ...newUser,
                          phoneNumber: event.target.value,
                        })
                      }
                      placeholder="Введить телефон"
                    />
                  </Form.Group>

                  <div className="d-flex justify-content-between">
                    <Form.Group>
                      <Form.Label>
                        Місто:
                        <span className="text-danger">*</span>
                      </Form.Label>
                      <Form.Control
                        tabIndex={0}
                        value={newUser.city}
                        onChange={(event) =>
                          setNewUser({ ...newUser, city: event.target.value })
                        }
                        placeholder="Введить місто"
                      />
                    </Form.Group>

                    <Form.Group className="mx-2 w-25">
                      <Form.Label>Років:</Form.Label>
                      <Form.Control
                        tabIndex={0}
                        value={newUser.age}
                        onChange={(event) => {
                          setNewUser({
                            ...newUser,
                            age: Number(
                              event.target.value.replace(/[^0-9]/g, "")
                            ),
                          });
                        }}
                        placeholder="0"
                      />
                    </Form.Group>

                    <Form.Group className="w-25">
                      <Form.Label>Діти:</Form.Label>
                      <Form.Control
                        tabIndex={0}
                        value={newUser.children}
                        onChange={(event) => {
                          setNewUser({
                            ...newUser,
                            children: Number(
                              event.target.value.replace(/[^0-9]/g, "")
                            ),
                          });
                        }}
                        placeholder="0"
                      />
                    </Form.Group>
                  </div>

                  <Form.Group>
                    <Form.Label>Коментар:</Form.Label>
                    <Form.Control
                      tabIndex={0}
                      value={newUser.comments}
                      onChange={(event) =>
                        setNewUser({ ...newUser, comments: event.target.value })
                      }
                      placeholder="Введить коментар"
                    />
                  </Form.Group>
                  <div className="d-flex justify-content-between">
                    <Form.Group>
                      <Form.Label>Пункт видачі:</Form.Label>
                      <DistributionPointSelect
                        selected={newUser.distributionPoint}
                        handleSelect={(value) =>
                          setNewUser({ ...newUser, distributionPoint: value })
                        }
                      />
                    </Form.Group>

                    <Form.Group className="mx-1">
                      <Form.Label>Дата видачі:</Form.Label>
                      <Form.Control
                        disabled={!newUser.isReceived}
                        type="date"
                        value={dayjs().format(DATE_FORMAT)}
                        onChange={(e) =>
                          setNewUser({
                            ...newUser,
                            lastReceivedDate: dayjs(e.target.value).format(
                              DATE_FORMAT
                            ),
                          })
                        }
                      />
                    </Form.Group>

                    <Form.Group>
                      <Form.Label>Номер групи:</Form.Label>
                      <RecipientGroupSelect
                        selected={newUser.group}
                        handleSelect={(value) =>
                          setNewUser({ ...newUser, group: Number(value) })
                        }
                      />
                    </Form.Group>
                  </div>

                  <Form.Check
                    onChange={() =>
                      setNewUser({
                        ...newUser,
                        isReceived: !newUser.isReceived,
                      })
                    }
                    checked={newUser.isReceived}
                    label="Отримав продуктовий набір сьогодні"
                  />

                  <Multiselect
                    selectedValues={newUser.additional}
                    options={RecipientAdditionalOptions}
                    displayValue="item"
                    emptyRecordMsg="Немає плюшок"
                    showArrow
                    placeholder="Додактово отримав"
                    closeIcon="cancel"
                    closeOnSelect
                    onSelect={(selectedList) => {
                      setNewUser({ ...newUser, additional: selectedList });
                    }}
                    onRemove={(selectedList) => {
                      setNewUser({ ...newUser, additional: selectedList });
                    }}
                  />
                </Form>
              </Card.Body>
            </Card>
          )}
        </Modal.Body>

        <Modal.Footer>
          <Button
            tabIndex={1}
            variant="secondary"
            className="mx-1"
            onClick={handleClose}
          >
            Закрити
          </Button>
          <Button
            tabIndex={0}
            variant="primary"
            disabled={isCreateDisabled}
            onClick={handleCreateNewUser}
          >
            Створити
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
