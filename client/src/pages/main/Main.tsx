import { ChangeEvent, useEffect, useMemo, useState } from "react";
import { Button, Card, Container, Form, InputGroup } from "react-bootstrap";
import { toast } from "react-toastify";
import logo from "../../assets/logo.jpeg";
import CreateRecipientButton from "../../components/CreateRecipientButton";
import { DistributionPointSelect } from "../../components/DistributionPointSelect";
import ImportXLSXFileTOGeneralDB from "../../components/ImportXLSXFileGeneralDB";
import { RecipientsTable } from "../../components/RecipientsTable";
import { EDistributionPoint } from "../../enums/distribution-point";
import { recipientDataService } from "../../services/recipients-data-service";
import { IRecipient } from "../../types/recipient";

export interface Props {
  updated: () => void;
}

function Main(props: Props) {
  const [recipientList, setRecipientList] = useState<IRecipient[]>([]);
  const [searchField, setSearchField] = useState<keyof IRecipient>("name");
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [selectedPoint, setSelectedPoint] = useState(EDistributionPoint.Filter);
  const searchFieldOptions: { value: keyof IRecipient; label: string }[] = [
    { value: "name", label: "ПІБ" },
    { value: "phoneNumber", label: "Телефон" },
  ];

  useEffect(() => {
    const value = (sessionStorage.getItem("distributionPoint") ||
      localStorage.getItem("distributionPoint")) as
      | EDistributionPoint
      | undefined;
    if (value) setSelectedPoint(value);
  }, []);

  const isSearchButtonDisabled = useMemo<boolean>(
    () => searchQuery === "",
    [searchQuery]
  );
  const isRecipientFinded = useMemo<boolean>(
    () => !!recipientList.length,
    [recipientList.length]
  );

  function handleSelectSearchField(e: any) {
    e.preventDefault();
    setSearchField(e.target.value);
  }

  async function handleSearchRecipients(e?: any) {
    e && e.preventDefault();

    try {
      const {
        data: { results },
      } = await recipientDataService.getAll({
        [searchField]: searchQuery.trim(),
      });

      setRecipientList(results);
    } catch (err: any) {
      toast.warning(err.response.data.error, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    }
  }

  function handleUpdateUserList(updatedUserList: IRecipient[]) {
    setRecipientList(updatedUserList);
  }

  function handleInputName(
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    e.preventDefault();
    setSearchQuery(e.target.value);

    if (e.target.value === "") {
      setRecipientList([]);
    }
  }

  function handleSelect(value: EDistributionPoint) {
    setSelectedPoint(value);
    sessionStorage.setItem("distributionPoint", value);
    localStorage.setItem("distributionPoint", value);
  }

  function handlePresEnter(e: any) {
    if (e.key === "Enter") {
      e.preventDefault();

      handleSearchRecipients();
    }
  }

  return (
    <Container
      fluid
      className="mt-5 d-flex align-items-center justify-content-center flex-column gap-2"
    >
      <div style={{ width: "600px" }} className="d-flex flex-column gap-2 mb-2">
        <div className="d-flex gap-3">
          <Form.Group className="w-100">
            <DistributionPointSelect
              selected={selectedPoint}
              handleSelect={handleSelect}
            />
          </Form.Group>
          <ImportXLSXFileTOGeneralDB imported={props.updated} />
        </div>
        <CreateRecipientButton selectedPoint={selectedPoint} />
        <Card>
          <Card.Img variant="top" src={logo} height={200} />

          <Card.Body>
            <Form
              onSubmit={(e) => {
                e.preventDefault();
              }}
            >
              <Form.Group className="d-flex flex-column gap-1">
                <Form.Label>
                  Введить прізвище або номер телефону для пошуку у базі:
                </Form.Label>
                <InputGroup>
                  <Form.Control
                    autoFocus
                    value={searchQuery}
                    onChange={handleInputName}
                    onKeyPress={handlePresEnter}
                    placeholder="Введить прізвище або номер телефону"
                  />

                  <Form.Select
                    className="search-field-select"
                    value={searchField}
                    onChange={handleSelectSearchField}
                  >
                    {searchFieldOptions.map((option) => (
                      <option key={option.value} value={option.value}>
                        {option.label}
                      </option>
                    ))}
                  </Form.Select>

                  <Button
                    variant="primary"
                    disabled={isSearchButtonDisabled}
                    onClick={handleSearchRecipients}
                  >
                    Шукати
                  </Button>
                </InputGroup>
              </Form.Group>
            </Form>
          </Card.Body>
        </Card>
      </div>

      {isRecipientFinded && (
        <RecipientsTable
          selectedPoint={selectedPoint}
          recipientList={recipientList}
          handleUpdateUserList={handleUpdateUserList}
        />
      )}
    </Container>
  );
}

export default Main;
