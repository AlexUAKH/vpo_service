import { useState } from "react";
import {
  Badge,
  Button,
  Container,
  Form,
  FormCheck,
  Spinner,
  Table,
} from "react-bootstrap";
import { toast } from "react-toastify";
import RecipientEditModal from "../../components/RecipientEditModal";
import { RecipientGroupSelect } from "../../components/RecipientGroupSelect";
import { DATE_FORMAT } from "../../constants/formats";
import {
  distributionPointOptions,
  recipientSortingOptions,
} from "../../constants/recipients";
import { EColors } from "../../enums/colors";
import { EDistributionPoint } from "../../enums/distribution-point";
import { ERecipientGroups } from "../../enums/recipient-group";
import { ESortRecipient } from "../../enums/sort";
import dayjs from "../../plugins/dayjs";
import { recipientDataService } from "../../services/recipients-data-service";
import { IAdditionalOption } from "../../types/additonalOptions";
import { IRecipient } from "../../types/recipient";

export default function QueryList() {
  // const today = dayjs();

  const [periodStart, setPeriodStart] = useState<string | undefined>(undefined);
  const [periodEnd, setPeriodEnd] = useState<string | undefined>(
    dayjs().format()
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [recipientList, setRecipients] = useState<IRecipient[]>([]);
  const [isReceived, setIsReceived] = useState<boolean>(true);
  const [distributionPoint, setDistributionPoint] = useState<
    EDistributionPoint | ""
  >("");
  const [sorting, setSorting] = useState<ESortRecipient>(
    ESortRecipient.Ascending
  );
  const [group, setGroup] = useState(ERecipientGroups.Default);

  const [showEdit, setShowEdit] = useState<boolean>(false);
  const [editedRecipient, setEditedRecipient] = useState({} as IRecipient);

  // const lastReceivedDateOptions = [
  //   {
  //     value: "",
  //     label: "Всі",
  //   },
  //   {
  //     value: today.subtract(2, "w").utc(true).format(DATE_FORMAT),
  //     label: "2",
  //   },
  //   {
  //     value: today.subtract(3, "w").utc(true).format(DATE_FORMAT),
  //     label: "3",
  //   },
  //   {
  //     value: today.subtract(4, "w").utc(true).format(DATE_FORMAT),
  //     label: "4",
  //   },
  //   {
  //     value: today.subtract(5, "w").utc(true).format(DATE_FORMAT),
  //     label: "5",
  //   },
  //   {
  //     value: today.subtract(36, "d").utc(true).format(DATE_FORMAT),
  //     label: "Більше",
  //   },
  // ];
  const DPOptions = [
    { value: "", label: "Пункт розачі ---" },
    ...distributionPointOptions,
  ];
  const sortingOptions = [
    // { value: ESortRecipient.Origin, label: "Сортування" },
    ...recipientSortingOptions,
  ];

  function handleSelectDistributionPoint(e: any) {
    e.preventDefault();
    setDistributionPoint(e.target.value);
  }

  function handleSelectSorting(e: any) {
    e.preventDefault();
    setSorting(e.target.value);
  }

  async function handleSearch() {
    try {
      setLoading(true);
      const {
        data: { results },
      } = await recipientDataService.getAll({
        isReceived,
        ...(distributionPoint && { distributionPoint }),
        ...(sorting && { sorting }),
        ...(group && { group }),
        ...(periodStart && {
          periodStart: dayjs(periodStart).format(DATE_FORMAT),
        }),
        ...(periodEnd && { periodEnd: dayjs(periodEnd).format(DATE_FORMAT) }),
      });

      setRecipients(results);
    } catch (err: any) {
      toast.warning(err.response.data.error, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    } finally {
      setLoading(false);
    }
  }

  function getDate(date: null | string | Date | dayjs.Dayjs): string | null {
    return date ? dayjs(date).format(DATE_FORMAT) : date;
  }

  function handleCopyToBuffer() {
    const copyText = recipientList
      .map((item, index) =>
        Object.values({
          index,
          name: item.name,
          phoneNumber: item.phoneNumber,
        }).join("    ")
      )
      .join("\n\n");
    navigator.clipboard.writeText(copyText).catch((error) => {
      toast.warn(error.messages, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        progress: undefined,
      });
    });
  }

  function handleEdit(recipient: IRecipient) {
    setEditedRecipient(recipient);
    setShowEdit(true);
  }

  function handleUpdate(recipient: any) {
    const newList = [...recipientList];
    const index = newList.findIndex((item) => item._id === recipient._id);
    newList.splice(index, 1, recipient);
    setRecipients(newList);
  }

  return (
    <>
      <Container fluid>
        <div style={{ maxWidth: "800px" }} className="mx-auto overflow-auto">
          <Button className="w-100" onClick={handleSearch}>
            Шукати
          </Button>

          <div className="d-flex gap-4 align-items-center mt-4 m-2">
            <Form.Check
              checked={isReceived}
              onChange={() => setIsReceived(!isReceived)}
              label="Отримував"
            />

            <div style={{ maxWidth: "175px" }}>
              <Form.Select
                value={distributionPoint}
                onChange={handleSelectDistributionPoint}
              >
                {DPOptions.map((option) => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </Form.Select>
            </div>

            <div style={{ maxWidth: "175px" }}>
              <Form.Select value={sorting} onChange={handleSelectSorting}>
                {sortingOptions.map((option) => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </Form.Select>
            </div>
            <div style={{ maxWidth: "175px" }}>
              <RecipientGroupSelect
                selected={group}
                handleSelect={(value) => setGroup(Number(value))}
              />
            </div>
          </div>

          <div className="d-flex gap-4 align-items-center mb-4">
            <div className="text-nowrap">Період з:</div>
            <Form.Control
              type="date"
              value={dayjs(periodStart).format(DATE_FORMAT)}
              onChange={(e) =>
                setPeriodStart(dayjs(e.target.value).format(DATE_FORMAT))
              }
            />
            <div>по:</div>
            <Form.Control
              type="date"
              value={dayjs(periodEnd).format(DATE_FORMAT)}
              onChange={(e) =>
                setPeriodEnd(dayjs(e.target.value).format(DATE_FORMAT))
              }
            />
          </div>
        </div>

        {loading ? (
          <Spinner animation="border" />
        ) : (
          <div className="table-responsive">
            <Table bordered striped hover>
              <thead>
                <tr>
                  <th>
                    <Button
                      onClick={handleCopyToBuffer}
                      size="sm"
                      variant="link"
                      className="p-0"
                      title="Копіювати данні з таблиці"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-clipboard"
                        viewBox="0 0 16 16"
                      >
                        <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                        <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                      </svg>
                    </Button>
                  </th>
                  <th>#</th>
                  <th>ПІБ</th>
                  <th>Номер</th>
                  <th>Місто</th>
                  <th className="text-center">Вік</th>
                  <th className="text-center">Діти</th>
                  <th className="text-center">
                    Коли отримав/
                    <br />
                    зареєструвався
                  </th>
                  <th className="text-center">Додатково отримав(ла)</th>
                  <th>Пункт отримання</th>
                  <th className="text-center">Група</th>
                </tr>
              </thead>
              <tbody>
                {(recipientList || []).map((recipient, index) => (
                  <tr
                    key={recipient._id}
                    onDoubleClick={() => handleEdit(recipient)}
                  >
                    <td
                      style={{
                        background: recipient.blackList ? EColors.Black : "",
                      }}
                    >
                      <FormCheck />
                    </td>
                    <td>{index + 1}</td>
                    <td>{recipient.name}</td>
                    <td>{recipient.phoneNumber}</td>
                    <td>{recipient.city}</td>
                    <td className="text-center">{recipient.age || "-"}</td>
                    <td className="text-center">{recipient.children || "-"}</td>
                    <td className="text-center">
                      {getDate(recipient.lastReceivedDate)}
                    </td>
                    <td className="text-center">
                      {recipient.additional && recipient.additional.length > 0
                        ? recipient.additional.map(
                            (item: IAdditionalOption) => {
                              return (
                                <Badge
                                  pill
                                  bg="primary"
                                  key={item.id}
                                  className="mx-1"
                                >
                                  {item.item}
                                </Badge>
                              );
                            }
                          )
                        : "-"}
                    </td>
                    <td>{recipient.distributionPoint}</td>
                    <td className="text-center">{recipient.group}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        )}
      </Container>
      <RecipientEditModal
        show={showEdit}
        recipient={editedRecipient}
        close={() => setShowEdit(false)}
        update={handleUpdate}
      />
    </>
  );
}
