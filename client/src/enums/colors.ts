export enum EColors {
  Red = "#dc3545",
  Yellow = "#ffc107",
  Green = "#198754",
  Black = "#000000",
  Default = "#6c757d",
}
