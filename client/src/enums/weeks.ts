export enum EWeeks {
  OneWeek = "1_week",
  TwoWeeks = "2_weeks",
  More = "more",
}
