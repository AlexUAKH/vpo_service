export enum ESortRecipient {
  Ascending = 1,
  Descending = -1,
  Origin = 0,
  Group = 2,
}
