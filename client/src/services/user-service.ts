import jwt from "jwt-decode";
import { ILoginData } from "../components/LoginForm";
import { authHttp, http } from "../plugins/axios";
import { IUserData } from "../types/user";

class UserService {
  tableName = "statistic";

  login(data: ILoginData): Promise<IUserData> {
    return new Promise((resolve, reject) => {
      http
        .post("auth/login", data)
        .then((result) => {
          const token = result.data.token;
          const decodedToken = jwt(token);
          const refreshToken = result.data.refresh_token;
          const decodedRefreshToken = jwt(refreshToken);
          const { exp: tokenExpire, iat, ...profile }: any = decodedToken;
          const { exp: refreshTokenExpire }: any = decodedRefreshToken;
          const _user: IUserData = {
            token,
            refreshToken,
            tokenExpire,
            refreshTokenExpire,
            profile,
          };
          resolve(_user);
        })
        .catch((e) => {
          reject(e);
        });
    });
  }
  refresh() {
    return authHttp.get("auth/refresh");
  }
}

export const userService = new UserService();
