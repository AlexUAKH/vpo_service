import { authHttp, http } from "../plugins/axios";
import { IRecipient } from "../types/recipient";

export type Queries = Record<keyof IRecipient | string, any>;
export type Paginated<T> = { results: T[] };

class RecipientDataService {
  tableName = "recipient";

  async importToDB(recipients: IRecipient[]) {
    await authHttp.post(`/import`, recipients);
  }

  getAll(params: Queries) {
    return http.get<Paginated<IRecipient>>(`${this.tableName}`, { params });
  }

  getById(id: string | number) {
    return http.get<IRecipient>(`${this.tableName}/${id}`);
  }

  create(recipient: IRecipient) {
    return authHttp.post(`${this.tableName}`, recipient);
  }

  async update(recipient: IRecipient) {
    await authHttp.put<IRecipient>(`${this.tableName}`, recipient);
    return recipient;
  }

  delete(id: string) {
    return authHttp.delete(`${this.tableName}/${id}`);
  }
}

export const recipientDataService = new RecipientDataService();
