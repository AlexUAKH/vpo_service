import { http } from "../plugins/axios";
import { Statistic } from "../types/statistic";

class StatisticDataService {
  tableName = "statistic";

  getStatistic() {
    return http.get<Statistic>(`${this.tableName}`);
  }
  getStatistic_d() {
    return http.get<Statistic>(`${this.tableName}`);
  }
}

export const statisticDataService = new StatisticDataService();
