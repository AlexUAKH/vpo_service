import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type UserDocument = User & Document;

@Schema()
export class User {
  @ApiProperty({ example: 'Jhon', description: 'User name' })
  @Prop()
  name: string;

  @ApiProperty({
    example: '+380964527814',
    description: 'User phone number'
  })
  @Prop()
  phoneNumber: string;

  @ApiProperty({
    example: 'user@email.com',
    description: "User's email"
  })
  @Prop({ unique: true })
  email: string;

  @ApiProperty({
    example: '**********',
    description: 'User password'
  })
  @Prop()
  password: string;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'created date'
  })
  @Prop()
  created_at: Date;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'updated date'
  })
  @Prop()
  updated_at: Date;
}
export const UserModel = SchemaFactory.createForClass(User);
