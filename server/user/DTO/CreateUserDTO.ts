import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDTO {
  @ApiProperty({ example: 'Den', description: "Admin's name" })
  name?: string;

  @ApiProperty({
    example: '+380964527814',
    description: "Admin's phone number"
  })
  readonly phoneNumber?: string;

  @ApiProperty({
    example: "Admin's email",
    description: 'Email',
    required: true
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    example: '2531cvr31t53',
    description: "Admin's password"
  })
  @IsNotEmpty()
  password: string;
}
