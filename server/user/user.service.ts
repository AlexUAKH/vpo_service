import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model, ObjectId } from 'mongoose';
import { ErrorHandler } from '../errors/errorHandler';
import { CreateUserDTO } from './DTO/CreateUserDTO';
import { User, UserDocument } from './user.model';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>
  ) {}

  async findById(id: ObjectId) {
    return await this.userModel.findById(id);
  }
  async findOne(user: any) {
    return await this.userModel.findOne(user);
  }

  async create({
    email,
    password,
    phoneNumber = '911',
    name = 'user'
  }: CreateUserDTO) {
    if (!email && !password) {
      ErrorHandler.badRequest("Email та password обов'язкові поля");
    }

    const userExist = await this.userModel.findOne({ email });

    if (userExist) {
      ErrorHandler.badRequest('Email вже використовується');
    }

    const hashPassword = await bcrypt.hash(password, 5);
    const date = new Date().toISOString();
    return await this.userModel.create({
      email,
      password: hashPassword,
      name,
      phoneNumber,
      created_at: date,
      updated_at: date
    });
  }
}
