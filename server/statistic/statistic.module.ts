import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Recipient, RecipientModel } from '../recipient/recipient.model';
import { StatisticController } from './statistic.controller';
import { StatisticService } from './statistic.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Recipient.name, schema: RecipientModel }
    ])
  ],
  controllers: [StatisticController],
  providers: [StatisticService]
})
export class StatisticModule {}
