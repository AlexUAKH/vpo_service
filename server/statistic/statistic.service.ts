import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Recipient, RecipientDocument } from '../recipient/recipient.model';
import { IPeriodQuery } from './statistic.controller';

@Injectable()
export class StatisticService {
  constructor(
    @InjectModel(Recipient.name)
    private recipientModel: Model<RecipientDocument>
  ) {}

  async statistic() {
    return Promise.allSettled([
      this.day(),
      this.month(),
      this.year(),
      this.total()
    ]).then((results) => ({
      day: results[0].status === 'fulfilled' ? results[0].value : -1,
      month: results[1].status === 'fulfilled' ? results[1].value : -1,
      year: results[2].status === 'fulfilled' ? results[2].value : -1,
      total: results[3].status === 'fulfilled' ? results[3].value : -1
    }));
    // return {
    //   day: await this.day(),
    //   month: await this.month(),
    //   year: await this.year()
    // };
  }

  async day() {
    const day = new Date().toISOString().split('T')[0];
    const dayStart = day + 'T00:00:00.000Z';

    return await this.recipientModel
      .find({
        lastReceivedDate: { $gte: dayStart },
        isReceived: true
      })
      .count();
  }

  async month() {
    // const month = new Date().toISOString().split('T')[0].split('-');
    // const monthStart = `${month[0]}-${month[1]}-01T00:00:00.000Z`;
    const currentDate = Date.now();
    const month = 30 * 24 * 60 * 60 * 1000;
    const monthBack = currentDate - month;
    const monthStart = new Date(monthBack).toISOString();

    return await this.recipientModel
      .find({
        lastReceivedDate: { $gte: monthStart },
        isReceived: true
      })
      .count();
  }

  async year() {
    // const year = new Date().toISOString().split('T')[0].split('-');
    // const yearStart = `${year[0]}-01-01T00:00:00.000Z`;

    const currentDate = Date.now();
    const year = 365 * 24 * 60 * 60 * 1000;
    const yearBack = currentDate - year;
    const yearStart = new Date(yearBack).toISOString();

    return await this.recipientModel
      .find({
        lastReceivedDate: { $gte: yearStart },
        isReceived: true
      })
      .count();
  }

  async total() {
    return await this.recipientModel.find().count();
  }

  async period(period: IPeriodQuery): Promise<number> {
    const { start, end } = period;

    const lastReceivedDate = {};

    if (start) {
      const day = start.split('T')[0];
      const dayStart = day + 'T00:00:00.000Z';
      lastReceivedDate['$gte'] = dayStart;
    }
    if (end) {
      const day = end.split('T')[0];
      const dayEnd = day + 'T23:59:59.999Z';
      lastReceivedDate['$lte'] = dayEnd;
    } else lastReceivedDate['$lte'] = new Date().toISOString();

    return await this.recipientModel.count({
      lastReceivedDate
    });
  }
}
