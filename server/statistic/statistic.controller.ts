import { Controller, Get, Query } from '@nestjs/common';
import {
  ApiOperation,
  ApiParam,
  ApiProperty,
  ApiResponse,
  ApiTags
} from '@nestjs/swagger';
import { StatisticService } from './statistic.service';

export interface IPeriodQuery {
  start: string;
  end: string;
}

export class IStatisticResponce {
  @ApiProperty({ example: 2, type: Number })
  day: number;

  @ApiProperty({ example: 27, type: Number })
  month: number;

  @ApiProperty({ example: 350, type: Number })
  year: number;
}

@ApiTags('Statistic')
@Controller('api/statistic')
export class StatisticController {
  constructor(private statisticService: StatisticService) {}

  @Get()
  @ApiOperation({ summary: 'Get statistic by one object' })
  @ApiResponse({ status: 200, type: IStatisticResponce })
  statistic() {
    return this.statisticService.statistic();
  }

  @Get('day')
  @ApiOperation({ summary: 'Get curent day statistic' })
  @ApiResponse({
    status: 200,
    description: 'Returns number of recipients who received help today'
  })
  day() {
    return this.statisticService.day();
  }

  @Get('month')
  @ApiOperation({ summary: 'Get last month statistic' })
  @ApiResponse({
    status: 200,
    description:
      'Returns number of recipients who received help from the begining of the current month'
  })
  month() {
    return this.statisticService.month();
  }

  @Get('year')
  @ApiOperation({ summary: 'Get year statistic' })
  @ApiResponse({
    status: 200,
    description:
      'Returns number of recipients who received help from 1st of January'
  })
  year(): Promise<any> {
    return this.statisticService.year();
  }

  @Get('period')
  @ApiOperation({ summary: 'Get pointed period statistic' })
  @ApiParam({ name: 'start', required: false })
  @ApiParam({ name: 'end', required: false })
  @ApiResponse({
    status: 200,
    description:
      'Returns count of peoples who received help within the pointed period'
  })
  period(@Query() query: IPeriodQuery) {
    return this.statisticService.period(query);
  }
}
