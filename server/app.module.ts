import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { RecipientModule } from './recipient/recipient.module';
import { StatisticModule } from './statistic/statistic.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client', 'build'),
      exclude: ['/api*']
    }),
    ConfigModule.forRoot({
      isGlobal: true
    }),
    MongooseModule.forRoot(
      process.env.NODE_ENV === 'development'
        ? process.env.DB_URL_DEV
        : process.env.DB_URL
    ),
    RecipientModule,
    StatisticModule,
    AuthModule,
    UserModule
  ]
})
export class AppModule {}

// 'mongodb+srv://vpoadmin:WsI1YV5XCw57CVf8@cluster0.5nd3dkt.mongodb.net/?retryWrites=true&w=majority'
//  ConfigModule.forRoot({
//       envFilePath: `.${process.env.NODE_ENV}.env`,
//       isGlobal: true
//     }),
