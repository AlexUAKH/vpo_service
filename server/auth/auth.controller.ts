import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  UseGuards
} from '@nestjs/common';
import { CreateUserDTO } from '../user/DTO/CreateUserDTO';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  async login(@Body() user: CreateUserDTO) {
    return this.authService.login(user);
  }

  @Post('registr')
  registr(@Body() user: CreateUserDTO) {
    return this.authService.registr(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile() {
    return this.authService.getProfile();
  }
}
