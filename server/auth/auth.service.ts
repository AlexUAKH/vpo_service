import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ObjectId } from 'mongoose';
import { ErrorHandler } from '../errors/errorHandler';
import { UserService } from '../user/user.service';

export interface IUser {
  email: string;
  password: string;
}
@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(id: ObjectId, pass: string): Promise<any> {
    const user = await this.userService.findOne(id);

    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login({ email, password }: IUser) {
    const userExist = await this.userService.findOne({ email });

    if (!userExist) ErrorHandler.badRequest(`Користувача ${email} не існує`);

    const comparePassword = await bcrypt.compare(password, userExist.password);

    if (!comparePassword) {
      ErrorHandler.badRequest('Невірний пароль');
    }

    const payload = { email: userExist.email, id: userExist._id };
    return {
      token: this.jwtService.sign(payload),
      refresh_token: this.jwtService.sign(payload, { expiresIn: '2h' })
    };
  }

  async registr(user: IUser) {
    const newUser = await this.userService.create(user);
    const payload = { email: newUser.email, _id: newUser._id };
    return {
      token: this.jwtService.sign(payload),
      refresh_token: this.jwtService.sign(payload, { expiresIn: '2h' })
    };
  }

  getProfile() {
    return { user: 'profile' };
  }
}
