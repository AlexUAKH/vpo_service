import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RecipientController } from './recipient.controller';
import { Recipient, RecipientModel } from './recipient.model';
import { RecipientService } from './recipient.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Recipient.name, schema: RecipientModel }
    ])
  ],
  providers: [RecipientService],
  controllers: [RecipientController]
})
export class RecipientModule {}
