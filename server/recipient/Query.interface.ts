export interface IQuery {
  phoneNumber?: string;
  name?: string;
  city?: string;
  limit?: number;
  page?: number;
  group?: number;
  isReceived?: boolean;
  distributionPoint?: string;
  comments?: string;
  lastReceivedDate?: string;
  periodStart?: string;
  periodEnd?: string;
  sorting?: ESortRecipient;
}

export enum ESortRecipient {
  Asc = 1,
  Desc = -1,
  Orig = 0,
  Group = 2
}
