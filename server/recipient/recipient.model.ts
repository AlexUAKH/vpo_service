import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { EDistributionPoint } from './DistributionPoint.enum';

export type RecipientDocument = Recipient & Document;

@Schema()
export class Recipient {
  @ApiProperty({ example: 'Jhon', description: 'Recipients name' })
  @Prop()
  name: string;

  @ApiProperty({
    example: '+380964527814',
    description: 'Recipients phone number'
  })
  @Prop({ unique: true })
  phoneNumber: string;

  @ApiProperty({
    example: 'Lviv',
    description: 'Recipients city'
  })
  @Prop()
  city: string;

  @ApiProperty({
    example: 'Tru lya lya',
    description: 'Comments'
  })
  @Prop()
  comments: string;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'Last receiving date'
  })
  @Prop()
  lastReceivedDate: Date;

  @ApiProperty({
    example: 'true',
    description: 'Receiving status'
  })
  @Prop({ default: false })
  isReceived: boolean;

  @ApiProperty({
    example: 'Городинського',
    description: 'Receiving place'
  })
  @Prop()
  distributionPoint: EDistributionPoint;

  @ApiProperty({
    example: 'true',
    description: 'Recipient in black list'
  })
  @Prop()
  blackList: boolean;

  @ApiProperty({
    example: '5',
    description: 'Recipient group'
  })
  @Prop()
  group: number;

  @ApiProperty({
    example: '1',
    description: 'Recipient age'
  })
  @Prop()
  age: number;

  @ApiProperty({
    example: '5',
    description: "Recipient's children"
  })
  @Prop()
  children: number;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'created date'
  })
  @Prop()
  created_at: Date;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'updated date'
  })
  @Prop()
  updated_at: Date;

  @ApiProperty({
    example: '[{item:"teapot",id:5}]',
    description: 'Received additionally',
    required: false
  })
  @Prop()
  additional: Array<IAdditionalOption>;
}
export const RecipientModel = SchemaFactory.createForClass(Recipient);

interface IAdditionalOption {
  item: string;
  id: number;
}
