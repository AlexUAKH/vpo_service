import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common';
import {
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiProperty,
  ApiResponse,
  ApiTags
} from '@nestjs/swagger';
import { ObjectId } from 'mongoose';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CreateDTO } from './DTO/CreateDTO';
import { ImportDTO } from './DTO/ImportDTO';
import { UpdateDTO } from './DTO/UpdateDTO';
import { IQuery } from './Query.interface';
import { Recipient } from './recipient.model';
import { RecipientService } from './recipient.service';

export class FindAllResponse {
  @ApiProperty({ example: 1, type: Number })
  count: number;

  @ApiProperty({ description: 'Recipients array', type: [Recipient] })
  results: any;
}
@ApiTags('Recipients')
@Controller('api')
export class RecipientController {
  constructor(private recipientService: RecipientService) {}

  @UseGuards(JwtAuthGuard)
  @Post('recipient')
  @ApiOperation({ summary: 'Create recipient' })
  @ApiResponse({ status: 201, type: Recipient })
  create(@Body() dto: CreateDTO): Promise<Recipient> {
    return this.recipientService.create(dto);
  }

  @Get('recipient')
  @ApiOperation({ summary: 'Find all recipient' })
  @ApiParam({ name: 'periodStart', required: false })
  @ApiParam({
    name: 'periodEnd',
    required: false,
    example: 'periodStart=2022-09-01T14:31:58.006Z'
  })
  @ApiParam({ name: 'distributionPoint', required: false })
  @ApiParam({
    name: 'lastReceivedDate',
    required: false,
    example:
      'lastReceivedDate=2022-09-01T14:31:58.006Z when using this field,   the period ignoring'
  })
  @ApiParam({ name: 'isReceived', required: false })
  @ApiParam({ name: 'comments', required: false })
  @ApiParam({ name: 'limit', required: false })
  @ApiParam({ name: 'page', required: false })
  @ApiParam({ name: 'city', required: false })
  @ApiParam({ name: 'name', required: false })
  @ApiParam({ name: 'phoneNumber', required: false })
  @ApiResponse({ status: 200, type: FindAllResponse })
  findAll(@Query() query: IQuery): Promise<FindAllResponse> {
    return this.recipientService.findAll(query);
  }

  @Get('recipient/:id')
  @ApiOperation({ summary: 'Find one recipient by id' })
  @ApiParam({ name: 'id' })
  @ApiResponse({ status: 200, type: Recipient })
  findOne(@Param('id') id: ObjectId) {
    return this.recipientService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('recipient/:id')
  @ApiOperation({ summary: 'Delete one recipient by id' })
  @ApiParam({ name: 'id' })
  @ApiResponse({ status: 200, type: Recipient })
  deleteById(@Param('id') id: ObjectId) {
    return this.recipientService.deleteById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Put('recipient')
  @ApiOperation({ summary: 'Update one recipient' })
  @ApiResponse({ status: 200, type: Recipient })
  update(@Body() dto: UpdateDTO) {
    return this.recipientService.update(dto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('import')
  @ApiOperation({ summary: 'Import recipients throu json' })
  @ApiBody({ type: [ImportDTO] })
  @ApiResponse({ status: 201, type: [Recipient] })
  importAll(@Body() body: ImportDTO[]) {
    return this.recipientService.importAll(body);
  }

  @UseGuards(JwtAuthGuard)
  @Post('add-field')
  @ApiOperation({ summary: 'Add field to table.' })
  @ApiBody({
    description: 'field - field name, value - field value'
  })
  @ApiResponse({ status: 201 })
  addField(@Body('field') field: string, @Body('value') value: any) {
    return this.recipientService.addField(field, value);
  }
}
