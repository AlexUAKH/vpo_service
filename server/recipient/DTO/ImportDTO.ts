import { ApiProperty } from '@nestjs/swagger';
import { CreateDTO } from './CreateDTO';

export class ImportDTO extends CreateDTO {
  id?: string;
  @ApiProperty({
    example: 'true',
    description: 'Receiving status',
    required: false
  })
  readonly isReceived?: boolean;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'created date'
  })
  created_at: string;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'updated date'
  })
  updated_at: string;
}
