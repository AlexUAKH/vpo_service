import { ApiProperty } from '@nestjs/swagger';
import { EDistributionPoint } from '../DistributionPoint.enum';
import { ERecipientGroups } from '../enums/ResipientGroup.enum';

export class CreateDTO {
  @ApiProperty({ example: 'Den', description: 'Recipients name' })
  readonly name: string;

  @ApiProperty({
    example: '+380964527814',
    description: 'Recipients phone number'
  })
  readonly phoneNumber: string;

  @ApiProperty({
    example: 'Lviv',
    description: 'Recipients city'
  })
  city: string;

  @ApiProperty({
    example: 'Tru lya lya',
    description: 'Comments',
    required: false
  })
  comments?: string;

  @ApiProperty({
    example: 'Городинського',
    description: 'Receiving place'
  })
  distributionPoint: EDistributionPoint;

  @ApiProperty({
    example: 'true',
    description: 'Receiving status',
    required: false
  })
  isReceived?: boolean;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'Last receiving date',
    required: false
  })
  lastReceivedDate: string;

  @ApiProperty({
    example: 'true',
    description: 'In balack list',
    required: false
  })
  blackList?: boolean;

  @ApiProperty({
    example: '5',
    description: 'Recipient group',
    required: false
  })
  group?: ERecipientGroups;

  @ApiProperty({
    example: '1',
    description: 'Recipient age',
    required: false
  })
  age?: ERecipientGroups;

  @ApiProperty({
    example: '2',
    description: "Recipient's children",
    required: false
  })
  children?: ERecipientGroups;

  @ApiProperty({
    example: '[{item:"teapot",id:5}]',
    description: 'Received additionally',
    required: false
  })
  additional?: any;
}
