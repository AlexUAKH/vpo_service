import { ApiProperty } from '@nestjs/swagger';
import { ObjectId } from 'mongoose';
import { CreateDTO } from './CreateDTO';

export class UpdateDTO extends CreateDTO {
  @ApiProperty({ example: 5, description: 'Unique identifier', type: 'string' })
  readonly _id: ObjectId;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'created date'
  })
  created_at: string;

  @ApiProperty({
    example: '2022-09-01T16:19:57.045Z',
    description: 'updated date'
  })
  updated_at: string;
}
