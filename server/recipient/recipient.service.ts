import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId } from 'mongoose';
import { ErrorHandler } from '../errors/errorHandler';
import { CreateDTO } from './DTO/CreateDTO';
import { ImportDTO } from './DTO/ImportDTO';
import { UpdateDTO } from './DTO/UpdateDTO';
import { ERecipientGroups } from './enums/ResipientGroup.enum';
import { ESortRecipient, IQuery } from './Query.interface';
import { Recipient, RecipientDocument } from './recipient.model';

@Injectable()
export class RecipientService {
  constructor(
    @InjectModel(Recipient.name)
    private recipientModel: Model<RecipientDocument>
  ) {}

  async create(dto: CreateDTO): Promise<Recipient> {
    const recipient = await this.recipientModel.findOne({
      phoneNumber: dto.phoneNumber
    });

    if (recipient)
      ErrorHandler.badRequest('Такий номер телефону вже викаристовується');

    const date = new Date().toISOString();
    const group = (dto.group && Number(dto.group)) || ERecipientGroups.Default;

    const record = {
      ...dto,
      blackList: dto.blackList || false,
      isReceived: dto.isReceived || false,
      lastReceivedDate: dto.lastReceivedDate || date,
      group,
      created_at: date,
      updated_at: date,
      additional: JSON.stringify(dto.additional)
    };
    if (dto.lastReceivedDate) {
      record['lastReceivedDate'] = dto.lastReceivedDate;
      record['isReceived'] = true;
    }

    const newRecipient = await this.recipientModel.create(record);

    return newRecipient;
  }

  async findAll(query: IQuery) {
    let {
      limit,
      page = 0,
      periodStart,
      periodEnd,
      isReceived,
      lastReceivedDate,
      sorting = ESortRecipient.Orig,
      group = ERecipientGroups.Default,
      ...arg
    } = query;
    const offset = limit ? page * limit : null;

    const filter = {};
    sorting = Number(sorting);
    group = Number(group);
    // prepearing filter object
    if (group) filter['group'] = group;

    // filter by isReceived field
    if (isReceived !== undefined)
      filter['isReceived'] = String(isReceived) === 'false' ? false : true;

    // filter by lastReceivedDate
    if (lastReceivedDate) {
      periodStart = lastReceivedDate;
      periodEnd = lastReceivedDate;
    }

    // fiter from start date to end date
    if (String(isReceived) !== 'false') {
      if (periodStart || periodEnd) {
        const lastReceivedDate = {};
        if (periodStart) {
          const day = periodStart.split('T')[0];
          const dayStart = day + 'T00:00:00.000Z';
          lastReceivedDate['$gte'] = dayStart;
        }
        if (periodEnd) {
          const day = periodEnd.split('T')[0];
          const dayEnd = day + 'T23:59:59.999Z';
          lastReceivedDate['$lte'] = dayEnd;
        } else lastReceivedDate['$lte'] = new Date().toISOString();

        filter['lastReceivedDate'] = lastReceivedDate;
      }
    }
    // filter by name, phoneNumber, city fields
    Object.keys(arg).forEach((filterKey) => {
      filter[filterKey] = { $regex: arg[filterKey], $options: 'si' };
    });

    // pagination
    const option = {
      offset,
      limit
    };

    const countPromise = this.recipientModel.find(filter).count();

    const recipientPromise = this.recipientModel.find(filter, null, option);

    return Promise.all([countPromise, recipientPromise]).then((values) => {
      const [count, docs] = values;
      let results = docs;

      if (sorting) {
        results = docs.sort((a, b) => {
          const d = Date.parse(String(b.lastReceivedDate));
          const f = Date.parse(String(a.lastReceivedDate));
          return sorting > 0 ? d - f : f - d;
        });
      }

      if (sorting === ESortRecipient.Group) {
        results = docs.sort((a, b) => {
          const d = Number(a.group);
          const f = Number(b.group);
          return d - f;
        });
      }

      const res = {
        count,
        results
      };

      return Promise.resolve(res);
    });
  }

  async findOne(id: ObjectId): Promise<Recipient> {
    return this.recipientModel.findById(id);
  }

  async deleteById(id: ObjectId): Promise<number> {
    const recipient = await this.recipientModel.findById(id);

    if (!recipient) ErrorHandler.notFound('Запису з таким id не існує');

    return await this.recipientModel.findByIdAndDelete(id);
  }

  async update(dto: UpdateDTO) {
    const recipient = await this.recipientModel.findOne({
      phoneNumber: dto.phoneNumber
    });

    if (recipient && recipient._id != dto._id)
      ErrorHandler.badRequest('Такий номер телефону вже викаристовується');

    return await this.recipientModel.findOneAndUpdate(
      { _id: dto._id },
      {
        ...dto,
        updated_at: new Date().toISOString()
      }
    );
  }

  async importAll(body: ImportDTO[]) {
    const checkedBody = this.checkCorrectDateFormat(body);

    const reductionedBody = this.removeDuplicates(checkedBody);

    // test for phoneNumber existance in DB
    const { toUpdate, toCreate } = await this.handleExistedRecipients(
      reductionedBody
    );

    toUpdate.forEach(async (el) => {
      await this.recipientModel.findOneAndUpdate(
        { phoneNumber: el.phoneNumber },
        el
      );
    });
    await this.recipientModel.create(toCreate);

    return { updated: toUpdate.length, created: toCreate.length };
  }

  async addField(field: string, value: string | number) {
    return this.recipientModel.updateMany({}, { $set: { [field]: value } });
  }

  async handleExistedRecipients(reductionedBody: ImportDTO[]) {
    const toUpdate = [];
    const toCreate = [];

    const recipients = await this.recipientModel.find({});

    reductionedBody.forEach((el) => {
      const existed = recipients.find(
        (recipient) => recipient.phoneNumber === el.phoneNumber
      );

      if (existed) {
        const up = this.updateFields(el, existed);
        toUpdate.push(up);
      } else {
        toCreate.push(el);
      }
    });

    return {
      toUpdate,
      toCreate
    };
  }

  checkCorrectDateFormat(body: ImportDTO[]): ImportDTO[] {
    const numRegex = /^-?\d+$/;

    return body.map((recipient) => {
      const date = recipient.lastReceivedDate;

      const isTimestamp = numRegex.test(date);
      if (isTimestamp)
        recipient.lastReceivedDate = new Date(Number(date)).toISOString();

      if (!recipient.created_at)
        recipient.created_at = new Date().toISOString();
      if (!recipient.updated_at)
        recipient.updated_at = new Date().toISOString();

      delete recipient.id;
      return recipient;
    });
  }

  removeDuplicates(arr: ImportDTO[]) {
    const result = [];
    for (let i = 0; i < arr.length; i++) {
      const duplicateIndex = result.findIndex(
        (el) => el.phoneNumber === arr[i].phoneNumber
      );
      if (duplicateIndex !== -1) {
        const duplicate = result[duplicateIndex];

        const updatedBody = this.updateFields(arr[i], duplicate);

        result[duplicateIndex] = updatedBody;
        arr.splice(i, 1);
        i--;
      } else {
        result.push(arr[i]);
      }
    }
    return result;
  }

  updateFields(first: any, second: any) {
    return {
      name: first.name || second.name,
      phoneNumber: first.phoneNumber,
      city: first.city || second.city,
      comments: first.comments || second.comments,
      lastReceivedDate:
        Date.parse(first.lastReceivedDate) >=
        Date.parse(second.lastReceivedDate)
          ? first.lastReceivedDate
          : second.lastReceivedDate,
      isReceived: first.isReceived || second.isReceived,
      distributionPoint: first.distributionPoint,
      created_at:
        Date.parse(first.created_at) <= Date.parse(second.created_at)
          ? first.created_at
          : second.created_at || new Date().toISOString(),
      updated_at:
        Date.parse(first.updated_at) >= Date.parse(second.updated_at)
          ? first.updated_at
          : second.updated_at || new Date().toISOString()
    };
  }
}
