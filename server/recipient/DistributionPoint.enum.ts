export enum EDistributionPoint {
  Filter = 'Фільтр',
  Reconciliation = 'Примирення',
  Gorodinskogo = 'Городинського',
}
